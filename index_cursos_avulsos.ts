import knex from 'knex'
import { connectionsConfig } from './connections.config'
import fs from 'fs'
import { subDays, subHours } from 'date-fns'

type EdukSubscription = {
  id: string
  subscriptionable_id: string
  plan_profile_id: number
  due_date: Date
  recurrency: boolean
  begin_date: Date
}

type ContentSubscription = {
  user_id: string
  valid_until?: Date | null
  created_at: 
}

type SubskriboSubscription = {
  user_id?: string
  legacy_id: string
  active_until: Date
  next_due_date: Date | null
  plan_id: string
  plan_profile_id: string
  begin_date: Date
}

type EdukCreditCard = {
  shopper_reference: string
  holder_name: string
  last_digits: string
  brand: string
  expires_at_month: number
  expires_at_year: number
  gateway_recurrence_contract: string
  default: number
  shopper_reference_type: string | null
  created_at: Date
}

type PagojCreditCard = {
  shopper_id?: string
  gateway_reference: string
  holder_name: string
  last_four_digits: string
  brand: string
  is_main: boolean
  expiration_date: string
  gateway_shopper_reference: string
}

type EdukUser = {
  id: string
  name: string
  email: string
  birthday: Date
  terms_of_use_accepted_at: Date | null
}

type OrkestroUser = {
  id?: string
  eduk_id: string
  name: string
  email: string
  birthdate: Date
  privacy_terms_accepted_at: Date
  deleted_at?: Date
  origin_application: 'eduk'
}

type MigrationData = {
  subscription: SubskriboSubscription
  user: OrkestroUser
}

type SubskriboUser = { id: string; name: string; email: string; has_main_card: boolean }

const paymentsDb = knex({
  client: 'mysql',
  connection: connectionsConfig.payments,
})

const subscriptionsDb = knex({
  client: 'pg',
  connection: connectionsConfig.subscriptions,
  searchPath: ['knex', 'public'],
})

const usersDb = knex({
  client: 'pg',
  connection: connectionsConfig.users,
  searchPath: ['knex', 'public'],
})

const orkestroDb = knex({
  client: 'pg',
  connection: connectionsConfig.orkestro,
  searchPath: ['knex', 'public'],
})

const subskriboDb = knex({
  client: 'pg',
  connection: connectionsConfig.subskribo,
  searchPath: ['knex', 'public'],
})

const pagojDb = knex({
  client: 'pg',
  connection: connectionsConfig.pagoj,
  searchPath: ['knex', 'public'],
})

const connections = [
  paymentsDb,
  subscriptionsDb,
  usersDb,
  orkestroDb,
  subskriboDb,
  pagojDb,
]

const PLAN_ID = process.env.PLAN_ID
const PLAN_PROFILE_ID = process.env.PLAN_PROFILE_ID

const safeFlag = process.env.SAFE_FLAG

async function main() {
  try {
    if (safeFlag === 'PROD') {
      return
    }

    const lifeTimeSubscriptions = await findLifeTimeContentSubscriptions()
    const users = await findSubscriptionUsers(lifeTimeSubscriptions)
    const userSubscriptions = await findUserSubscriptions(users)

    const migrationData = createMigrationData(
      userSubscriptions,
      users,
      lifeTimeSubscriptions
    )

    fs.writeFileSync('migration_data.json', JSON.stringify(migrationData))

    console.log('==> Migrando os dados')

    const jump = 2000
    for (let i = 0; i < migrationData.length; i += jump) {
      console.log(`de ${i} até ${migrationData.slice(i, i + jump).length + i}`)

      await migrateData(migrationData.slice(i, i + jump))
    }

    console.log('CHAMA NA GAMEPLAY PAPAI!!!')
  } catch (error) {
    console.log(error)
  } finally {
    await destroyConnections()
  }
}

async function findLifeTimeContentSubscriptions(): Promise<ContentSubscription[]> {
  console.log('==> Buscando todas as assinaturas ativas')

  const { rows: lifeTimeContentsSubscriptions } = await subscriptionsDb.raw<{
    rows: ContentSubscription[]
  }>(
    `
      SELECT distinct user_id
      FROM sales_purchased_contents
      WHERE valid_until is null or valid_until > '2025-01-01'
        AND status = 'active'
    `
  )

  console.log(`-> ${lifeTimeContentsSubscriptions.length} assinaturas encontradas`)

  return lifeTimeContentsSubscriptions
}

async function findUserSubscriptions(users: any[]): Promise<EdukSubscription[]> {
  console.log('==> Buscando as assinaturas dos usuários')

  const userIds = users.map(({ id }) => id)

  const subscriptions = await subscriptionsDb
    .select(
      'id',
      'subscriptionable_id',
      'plan_profile_id',
      'due_date',
      'recurrency',
      'begin_date'
    )
    .from('subscriptions')
    .whereIn('subscriptionable_id', userIds)
    .andWhere({
      status: 'active',
    })
    .andWhere('valid_until', '>', 'now()')

  console.log(`-> ${users.length} assinaturas encontrados`)

  return subscriptions
}

async function findSubscriptionUsers(contentsSubscriptions: ContentSubscription[]) {
  console.log('==> Buscando os usuários das assinaturas')

  const userIds = contentsSubscriptions.map(({ user_id }) => user_id)

  const users = await usersDb
    .select('id', 'name', 'email', 'birthday', 'terms_of_use_accepted_at')
    .from('v1_user')
    .whereIn('id', userIds)

  console.log(`-> ${users.length} usuários encontrados`)

  return users
}

function createMigrationData(
  subscriptions: any[],
  users: any[],
  contentSubscriptions: ContentSubscription[]
): MigrationData[] {
  console.log('==> Associando as assinaturas aos usuários')
  const migrationData: any[] = []

  users.forEach((user) => {
    const subscription = subscriptions.find(
      (subscription) => Number(user.id) === Number(subscription.subscriptionable_id)
    )

    const contentSubscription = contentSubscriptions.find(
      (contentSubscription) => Number(contentSubscription.user_id) === Number(user.id)
    )

    

    migrationData.push({
      user: mapToOrkestroUser(user),
      subscription: subscription ? mapToSubskriboSubscription(subscription) : null,
    })
  })

  console.log(`-> ${migrationData.length} assinaturas`)

  return migrationData
}

function mapToOrkestroUser(edukUser: EdukUser): OrkestroUser {
  return {
    eduk_id: edukUser.id,
    name: edukUser.name,
    email: edukUser.email,
    birthdate: edukUser.birthday,
    privacy_terms_accepted_at: edukUser.terms_of_use_accepted_at || new Date(),
    origin_application: 'eduk',
  }
}

function mapToSubskriboSubscription(
  edukSubscription: EdukSubscription
): SubskriboSubscription {
  return {
    legacy_id: edukSubscription.id,
    active_until: edukSubscription.due_date || subDays(new Date(), 2),
    next_due_date: edukSubscription.due_date || subDays(new Date(), 2),
    plan_id: PLAN_ID as string,
    plan_profile_id: PLAN_PROFILE_ID as string,
    begin_date: edukSubscription.begin_date,
  }
}

function mapContentSubscriptionToSubskriboSubscription(contentSubscription: ContentSubscription) {
  return {
    legacy_id: null,
    active_until: contentSubscription.valid_until ?? new Date('2023-09-02'),
    next_due_date: null,
    plan_id: PLAN_ID as string,
    plan_profile_id: PLAN_PROFILE_ID as string,
    begin_date: contentSubscription.created_at,
  }
}

async function migrateData(migrationData: MigrationData[]) {
  await Promise.all(
    migrationData.map(async ({ user, subscription }) => {
      const orkestroUser = await migrateUserToOkestro(user)

      const subskriboUser = await migrationUserToSubskribo(orkestroUser)

      subscription.user_id = orkestroUser.id
      await migrateSubscription(subscription)
    })
  )
}

async function migrateUserToOkestro(user: OrkestroUser): Promise<OrkestroUser> {
  const [orkestroUser] = await orkestroDb
    .from<OrkestroUser>('users')
    .where({ eduk_id: String(user.eduk_id) })

  if (orkestroUser) {
    return orkestroUser
  }

  const [createdOrkestroUser] = await orkestroDb.insert(user, '*').into('users')

  return createdOrkestroUser
}

async function migrationUserToSubskribo(user: OrkestroUser) {
  const [subskriboUser] = await subskriboDb.from('users').where({ id: user.id })

  if (subskriboUser) {
    return subskriboUser
  }

  const [createdSubskriboUser] = await subskriboDb
    .insert<SubskriboUser>({
      id: user.id as string,
      name: user.name,
      email: user.email,
      has_main_card: false,
    })
    .into('users')
    .returning('*')

  return createdSubskriboUser
}

async function migrateSubscription(subscription: SubskriboSubscription) {
  const [subskriboSubscription] = await subskriboDb.from('subscriptions').where({
    user_id: subscription.user_id,
    plan_profile_id: subscription.plan_profile_id,
  })

  if (subskriboSubscription) {
    return subskriboSubscription
  }

  const [createdSubskriboSubscription] = await subskriboDb
    .insert({
      user_id: subscription.user_id,
      active_until: subscription.active_until,
      next_due_date: null,
      plan_id: subscription.plan_id,
      plan_profile_id: subscription.plan_profile_id,
    })
    .into('subscriptions')
    .returning('*')

  const now = new Date()
  await subskriboDb
    .insert({
      plan_id: subscription.plan_id,
      activation_type: 'ACQUISITION',
      trigger: 'ACQUISITION',
      subscription_start_date: subscription.begin_date,
      subscription_end_date: subscription.active_until,
      payment_method: 'CREDIT_CARD',
      subscription_status: subscription.active_until > now ? 'ACTIVE' : 'SUSPENDED',
      automatic_renewal: !!subscription.next_due_date,
      status: 'done',
      order_id: null,
      subscription_id: createdSubskriboSubscription.id,
    })
    .into('subscription_history')

  await subskriboDb
    .insert({
      subscription_id: createdSubskriboSubscription.id,
      legacy_subscription_id: subscription.legacy_id,
    })
    .into('legacy_migrated_subscriptions')

  return createdSubskriboSubscription
}

async function migrateCreditCards(creditCards: PagojCreditCard[], shopperId: string) {
  const [mainCard] = await pagojDb
    .from('cards')
    .where({ shopper_id: shopperId, is_main: true })

  if (!mainCard) {
    creditCards.forEach((creditCard) => {
      creditCard.shopper_id = shopperId
    })

    await pagojDb.batchInsert('cards', creditCards)
  }
}

async function destroyConnections() {
  await Promise.all(connections.map((connection) => connection.destroy()))
}

main()
