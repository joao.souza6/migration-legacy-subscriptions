import * as dotenv from 'dotenv'

const environment = process.env.ENVIRONMENT

dotenv.config({ path: `.env.${environment}` })

export const connectionsConfig = {
  payments: {
    client: process.env.PAYMENTS_DB_CLIENT,
    host: process.env.PAYMENTS_DB_HOST,
    port: Number(process.env.PAYMENTS_DB_PORT),
    user: process.env.PAYMENTS_DB_USER,
    password: process.env.PAYMENTS_DB_PASSWORD,
    database: process.env.PAYMENTS_DB_DATABASE,
  },
  subscriptions: {
    host: process.env.SUBSCRIPTIONS_DB_HOST,
    port: Number(process.env.SUBSCRIPTIONS_DB_PORT),
    user: process.env.SUBSCRIPTIONS_DB_USER,
    password: process.env.SUBSCRIPTIONS_DB_PASSWORD,
    database: process.env.SUBSCRIPTIONS_DB_DATABASE,
  },
  users: {
    host: process.env.USERS_DB_HOST,
    port: Number(process.env.USERS_DB_PORT),
    user: process.env.USERS_DB_USER,
    password: process.env.USERS_DB_PASSWORD,
    database: process.env.USERS_DB_DATABASE,
  },
  orkestro: {
    host: process.env.ORKESTRO_DB_HOST,
    port: Number(process.env.ORKESTRO_DB_PORT),
    user: process.env.ORKESTRO_DB_USER,
    password: process.env.ORKESTRO_DB_PASSWORD,
    database: process.env.ORKESTRO_DB_DATABASE,
  },
  subskribo: {
    port: Number(process.env.SUBSKRIBO_DB_PORT),
    host: process.env.SUBSKRIBO_DB_HOST,
    user: process.env.SUBSKRIBO_DB_USER,
    password: process.env.SUBSKRIBO_DB_PASSWORD,
    database: process.env.SUBSKRIBO_DB_DATABASE,
  },
  pagoj: {
    port: Number(process.env.PAGOJ_DB_PORT),
    host: process.env.PAGOJ_DB_HOST,
    user: process.env.PAGOJ_DB_USER,
    password: process.env.PAGOJ_DB_PASSWORD,
    database: process.env.PAGOJ_DB_DATABASE,
  },
}
