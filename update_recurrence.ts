import knex from 'knex'
import { connectionsConfig } from './connections.config'
import { isUndefined } from 'lodash'
import fs from 'fs'

type EdukSubscription = {
  id: string
  subscriptionable_id: string
  plan_profile_id: number
  due_date: Date
  recurrency: boolean
  begin_date: Date
}

type MigratedSubscription = {
  id: string
  legacy_subscription_id: string
}

const subscriptionsDb = knex({
  client: 'pg',
  connection: connectionsConfig.subscriptions,
  searchPath: ['knex', 'public'],
})

const subskriboDb = knex({
  client: 'pg',
  connection: connectionsConfig.subskribo,
  searchPath: ['knex', 'public'],
})

const connections = [subscriptionsDb, subskriboDb]

const safeFlag = process.env.SAFE_FLAG

async function main() {
  try {
    if (safeFlag === 'PROD') {
      return
    }

    const subscriptions = await findSubskriboRecurringSubscriptions()

    const legacySubscriptions = await findLegacyNonRecurringSubscriptions(subscriptions)

    const subscriptionIds = legacySubscriptions
      .map((legacySubscription) => {
        const subscription = subscriptions.find(
          (subscription) => legacySubscription.id === subscription.legacy_subscription_id
        )

        return (subscription as MigratedSubscription).id
      })
      .filter((subscriptionId) => !isUndefined(subscriptionId))

    console.log(subscriptionIds.length)

    fs.writeFileSync('update_recurrence.json', JSON.stringify(subscriptionIds))

    // await subskriboDb
    //   .update({ next_due_date: null })
    //   .from('subscriptions')
    //   .whereIn('id', subscriptionIds as string[])

    // await subskriboDb
    //   .update({ automatic_renewal: false })
    //   .from('subscription_history')
    //   .whereIn('subscription_id', subscriptionIds as string[])

    console.log('CHAMA NA GAMEPLAY PAPAI!!!')
  } catch (error) {
    console.log(error)
  } finally {
    await destroyConnections()
  }
}

async function findSubskriboRecurringSubscriptions(): Promise<MigratedSubscription[]> {
  console.log('==> Buscando todas as assinaturas ativas')

  const { rows: subscriptions } = await subskriboDb.raw<{
    rows: MigratedSubscription[]
  }>(
    `
      SELECT subscriptions.id, lmc.legacy_subscription_id
      FROM subscriptions
              INNER JOIN legacy_migrated_subscriptions lmc ON lmc.subscription_id = subscriptions.id
      WHERE next_due_date is not null 
        AND subscriptions.id NOT IN (
          SELECT distinct sh.subscription_id
          FROM subscription_history sh
          WHERE sh.order_id is not null
            AND sh.payment_method = 'CREDIT_CARD'
      )
    `
  )

  console.log(`-> ${subscriptions.length} assinaturas encontradas`)

  return subscriptions
}

async function findLegacyNonRecurringSubscriptions(
  subscriptions: MigratedSubscription[]
): Promise<EdukSubscription[]> {
  console.log('==> Buscando assinaturas do legado')

  const legacySubscriptionIds = subscriptions.map(({ legacy_subscription_id }) =>
    Number(legacy_subscription_id)
  )

  const legacySubscriptions = await subscriptionsDb
    .select(
      'id',
      'subscriptionable_id',
      'plan_profile_id',
      'due_date',
      'recurrency',
      'begin_date'
    )
    .from('subscriptions')
    .where('recurrency', false)
    .whereIn('id', legacySubscriptionIds)

  console.log(`==> ${legacySubscriptions.length} legacySubscriptions encontradas`)

  return legacySubscriptions
}

async function destroyConnections() {
  await Promise.all(connections.map((connection) => connection.destroy()))
}

main()
