import knex from 'knex'
import { connectionsConfig } from './connections.config'
import { differenceInDays } from 'date-fns'

const paymentsDb = knex({
  client: 'mysql',
  connection: connectionsConfig.payments,
})

const subscriptionsDb = knex({
  client: 'pg',
  connection: connectionsConfig.subscriptions,
  searchPath: ['knex', 'public'],
})

const usersDb = knex({
  client: 'pg',
  connection: connectionsConfig.users,
  searchPath: ['knex', 'public'],
})

const orkestroDb = knex({
  client: 'pg',
  connection: connectionsConfig.orkestro,
  searchPath: ['knex', 'public'],
})

const subskriboDb = knex({
  client: 'pg',
  connection: connectionsConfig.subskribo,
  searchPath: ['knex', 'public'],
})

const pagojDb = knex({
  client: 'pg',
  connection: connectionsConfig.pagoj,
  searchPath: ['knex', 'public'],
})

const connections = [
  paymentsDb,
  subscriptionsDb,
  usersDb,
  orkestroDb,
  subskriboDb,
  pagojDb,
]

async function main() {
  try {
    const { legacySubscriptions, grSubscriptions } = await listLegacySubscriptions()

    // const users = await listSubscriptionUsers(legacySubscriptions)
    // console.log(`-> ${users.length} usuários com assinatura legada`)

    // const subscriptions = await listSubskriboSubscriptions(users)
    // console.log(`-> ${subscriptions.length} assinaturas no GR`)

    const updatedSubscriptions = await Promise.all(
      legacySubscriptions
        .map((legacySubscription: any) => {
          const subscription = grSubscriptions.find(
            ({ legacy_subscription_id }: any) =>
              legacySubscription.id === legacy_subscription_id
          )

          return {
            legacySubscription,
            subscription,
          }
        })
        .filter(
          ({ legacySubscription, subscription }: any) =>
            differenceInDays(legacySubscription.due_date, subscription.active_until) > 1
        )
        .map(async ({ legacySubscription, subscription }: Record<string, any>) => {
          const updateSubscriptionSql = await subskriboDb
            .update({
              active_until: legacySubscription.due_date,
              next_due_date: subscription.next_due_date && legacySubscription.due_date,
              updated_at: new Date(),
            })
            .from('subscriptions')
            .where('id', subscription.id)
            .returning('*')

          // cria histórico de renovação
          const now = new Date()
          const createHistorySql = await subskriboDb
            .insert({
              plan_id: subscription.plan_id,
              activation_type: 'RENOVATION',
              trigger: 'RENOVATION',
              subscription_start_date: subscription.active_until,
              subscription_end_date: legacySubscription.due_date,
              payment_method: 'CREDIT_CARD',
              subscription_status:
                legacySubscription.due_date > now ? 'ACTIVE' : 'SUSPENDED',
              automatic_renewal: !!subscription.next_due_date,
              status: 'done',
              order_id: null,
              subscription_id: subscription.id,
            })
            .into('subscription_history')
          return updateSubscriptionSql
        })
    )

    console.log(updatedSubscriptions.length)

    // fs.writeFileSync('data.json', JSON.stringify(updatedSubscriptions))
  } catch (error) {
    console.log(error)
  } finally {
    await destroyConnections()
  }
}

async function listLegacySubscriptions() {
  const { rows: grSubscriptions } = await subskriboDb.raw(`
    SELECT s.*, lms.legacy_subscription_id FROM subscriptions s
    INNER JOIN legacy_migrated_subscriptions lms ON s.id = lms.subscription_id
  `)

  const migratedLegacySubscriptionIds = grSubscriptions.map(
    ({ legacy_subscription_id }: any) => legacy_subscription_id
  )

  const legacySubscriptions = await subscriptionsDb
    .select('*')
    .from('subscriptions')
    .whereIn('id', migratedLegacySubscriptionIds)
    .whereNotIn(
      'id',
      [
        14071, 592818, 337034, 439176, 181460, 31498, 13067, 20486, 7084, 431352, 35766,
        9469, 191741, 177740, 569469, 76285, 307964, 572142, 64838, 86381, 7319, 572263,
      ]
    )

  return { legacySubscriptions, grSubscriptions }
}

// async function listSubscriptionUsers(subscriptions: any[]) {
//   const userIds = subscriptions.map(({ subscriptionable_id }) => subscriptionable_id)

//   return await orkestroDb
//     .select('id', 'eduk_id', 'name')
//     .from('users')
//     .whereIn('eduk_id', userIds)
// }

// async function listSubskriboSubscriptions(users: any[]) {
//   const userIds = users.map(({ id }) => id)

//   return await subskriboDb.select('*').from('subscriptions').whereIn('user_id', userIds)
// }

// async function listLegacySubscriptions(subscriptionUsers: any[]) {
//   const userIds = subscriptionUsers.map(({ eduk_id }) => eduk_id)

//   return await subscriptionsDb
//     .select('id', 'subscriptionable_id')
//     .from('subscriptions')
//     .whereNot({ status: 'active' })
//     .whereIn('subscriptionable_id', userIds)
// }

main()

async function destroyConnections() {
  await Promise.all(connections.map((connection) => connection.destroy()))
}
