import knex from 'knex'
import { connectionsConfig } from './connections.config'
import fs from 'fs'
import { uniqBy } from 'lodash'

type EdukSubscription = {
  id: string
  subscriptionable_id: string
  plan_profile_id: number
  due_date: Date
  recurrency: boolean
  begin_date: Date
}

type SubskriboSubscription = {
  user_id?: string
  legacy_id: string
  active_until: Date
  next_due_date: Date | null
  plan_id: string
  plan_profile_id: string
  begin_date: Date
}

type EdukCreditCard = {
  shopper_reference: string
  holder_name: string
  last_digits: string
  brand: string
  expires_at_month: number
  expires_at_year: number
  gateway_recurrence_contract: string
  default: number
  shopper_reference_type: string | null
  created_at: Date
}

type PagojCreditCard = {
  shopper_id?: string
  gateway_reference: string
  holder_name: string
  last_four_digits: string
  brand: string
  is_main: boolean
  expiration_date: string
  gateway_shopper_reference: string
}

type EdukUser = {
  id: string
  name: string
  email: string
  birthday: Date
  terms_of_use_accepted_at: Date | null
}

type OrkestroUser = {
  id?: string
  eduk_id: string
  name: string
  email: string
  birthdate: Date
  privacy_terms_accepted_at: Date
  deleted_at?: Date
  origin_application: 'eduk'
}

type MigrationData = {
  subscription: SubskriboSubscription
  user: OrkestroUser
  creditCards: PagojCreditCard[]
}

type SubskriboUser = { id: string; name: string; email: string; has_main_card: boolean }

const paymentsDb = knex({
  client: 'mysql',
  connection: connectionsConfig.payments,
})

const subscriptionsDb = knex({
  client: 'pg',
  connection: connectionsConfig.subscriptions,
  searchPath: ['knex', 'public'],
})

const usersDb = knex({
  client: 'pg',
  connection: connectionsConfig.users,
  searchPath: ['knex', 'public'],
})

const orkestroDb = knex({
  client: 'pg',
  connection: connectionsConfig.orkestro,
  searchPath: ['knex', 'public'],
})

const subskriboDb = knex({
  client: 'pg',
  connection: connectionsConfig.subskribo,
  searchPath: ['knex', 'public'],
})

const pagojDb = knex({
  client: 'pg',
  connection: connectionsConfig.pagoj,
  searchPath: ['knex', 'public'],
})

const connections = [
  paymentsDb,
  subscriptionsDb,
  usersDb,
  orkestroDb,
  subskriboDb,
  pagojDb,
]

async function main() {
  try {
    const legacySubscription = await listLegacySubscriptions()
    console.log(`-> ${legacySubscription.length} assinaturas legadas`)

    const users = await listSubscriptionUsers(legacySubscription)
    console.log(`-> ${users.length} usuários com assinatura legada`)

    const subscriptions = await listSubskriboSubscriptions(users)
    console.log(`-> ${subscriptions.length} assinaturas no GR`)

    console.log(
      users.filter(({ id }) => !subscriptions.some(({ user_id }) => user_id == id))
    )
  } catch (error) {
    console.log(error)
  } finally {
    await destroyConnections()
  }
}

async function listLegacySubscriptions() {
  const { rows: subscriptions } = await subscriptionsDb.raw(`
    SELECT id, subscriptionable_id, plan_profile_id, due_date, recurrency, begin_date
    FROM subscriptions
    WHERE status = 'active'
      AND type = 'Subscription'
      AND subscriptionable_id NOT IN (618193, 212916, 4733726, 2487148, 4684900, 4981009)
      AND created_at < '2022-09-01'
      AND subscriptionable_id NOT IN (
        SELECT distinct user_id
        FROM sales_purchased_contents
        WHERE valid_until > now()
        AND status = 'active')
    `)

  return subscriptions
}

async function listSubscriptionUsers(subscriptions: any[]) {
  const userIds = subscriptions.map(({ subscriptionable_id }) => subscriptionable_id)

  return await orkestroDb
    .select('id', 'eduk_id')
    .from('users')
    .whereIn('eduk_id', userIds)
}

async function listSubskriboSubscriptions(users: any[]) {
  const userIds = users.map(({ id }) => id)

  return await subskriboDb
    .select('id', 'user_id')
    .from('subscriptions')
    .whereIn('user_id', userIds)
}

// async function listLegacySubscriptions(subscriptionUsers: any[]) {
//   const userIds = subscriptionUsers.map(({ eduk_id }) => eduk_id)

//   return await subscriptionsDb
//     .select('id', 'subscriptionable_id')
//     .from('subscriptions')
//     .whereNot({ status: 'active' })
//     .whereIn('subscriptionable_id', userIds)
// }

main()

// ===========

async function destroyConnections() {
  await Promise.all(connections.map((connection) => connection.destroy()))
}
