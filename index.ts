import knex from 'knex'
import { connectionsConfig } from './connections.config'
import fs from 'fs'
import { subDays, subHours } from 'date-fns'

type EdukSubscription = {
  id: string
  subscriptionable_id: string
  plan_profile_id: number
  due_date: Date
  recurrency: boolean
  begin_date: Date
}

type SubskriboSubscription = {
  user_id?: string
  legacy_id: string
  active_until: Date
  next_due_date: Date | null
  plan_id: string
  plan_profile_id: string
  begin_date: Date
}

type EdukCreditCard = {
  shopper_reference: string
  holder_name: string
  last_digits: string
  brand: string
  expires_at_month: number
  expires_at_year: number
  gateway_recurrence_contract: string
  default: number
  shopper_reference_type: string | null
  created_at: Date
}

type PagojCreditCard = {
  shopper_id?: string
  gateway_reference: string
  holder_name: string
  last_four_digits: string
  brand: string
  is_main: boolean
  expiration_date: string
  gateway_shopper_reference: string
}

type EdukUser = {
  id: string
  name: string
  email: string
  birthday: Date
  terms_of_use_accepted_at: Date | null
}

type OrkestroUser = {
  id?: string
  eduk_id: string
  name: string
  email: string
  birthdate: Date
  privacy_terms_accepted_at: Date
  deleted_at?: Date
  origin_application: 'eduk'
}

type MigrationData = {
  subscription: SubskriboSubscription
  user: OrkestroUser
  creditCards: PagojCreditCard[]
}

type SubskriboUser = { id: string; name: string; email: string; has_main_card: boolean }

const paymentsDb = knex({
  client: 'mysql',
  connection: connectionsConfig.payments,
})

const subscriptionsDb = knex({
  client: 'pg',
  connection: connectionsConfig.subscriptions,
  searchPath: ['knex', 'public'],
})

const usersDb = knex({
  client: 'pg',
  connection: connectionsConfig.users,
  searchPath: ['knex', 'public'],
})

const orkestroDb = knex({
  client: 'pg',
  connection: connectionsConfig.orkestro,
  searchPath: ['knex', 'public'],
})

const subskriboDb = knex({
  client: 'pg',
  connection: connectionsConfig.subskribo,
  searchPath: ['knex', 'public'],
})

const pagojDb = knex({
  client: 'pg',
  connection: connectionsConfig.pagoj,
  searchPath: ['knex', 'public'],
})

const connections = [
  paymentsDb,
  subscriptionsDb,
  usersDb,
  orkestroDb,
  subskriboDb,
  pagojDb,
]

const PLAN_ID = process.env.PLAN_ID
const PLAN_PROFILE_ID = process.env.PLAN_PROFILE_ID

const safeFlag = process.env.SAFE_FLAG

async function main() {
  try {
    if (safeFlag === 'PROD') {
      return
    }

    const subscriptions = await findActiveSubscriptions()

    const users = await findSubscriptionUsers(subscriptions)
    const creditCards = await findCreditCardBySubscriptions(subscriptions)
    const usersCreditCards = await findCreditCardBySubscriptionUsers(subscriptions)

    const migrationData = associateSubscriptionWithCreditCards(
      subscriptions,
      creditCards,
      usersCreditCards,
      users
    )

    fs.writeFileSync('migration_data.json', JSON.stringify(migrationData))

    console.log('==> Migrando os dados')

    const jump = 2000
    for (let i = 0; i < migrationData.length; i += jump) {
      console.log(`de ${i} até ${migrationData.slice(i, i + jump).length + i}`)

      await migrateData(migrationData.slice(i, i + jump))
    }

    console.log('CHAMA NA GAMEPLAY PAPAI!!!')
  } catch (error) {
    console.log(error)
  } finally {
    await destroyConnections()
  }
}

async function findActiveSubscriptions() {
  console.log('==> Buscando todas as assinaturas ativas')

  // Usuários com assinaturas de PLANO ativas e não tem assinatura de CONTEÚDOS AVULSOS ativa
  const { rows: subscriptions } = await subscriptionsDb.raw<{
    rows: EdukSubscription[]
  }>(
    `
    SELECT id, subscriptionable_id, plan_profile_id, due_date, recurrency, begin_date
    FROM subscriptions
    WHERE status = 'active'
      AND plan_profile_id = 99
      AND type = 'Subscription'
      AND subscriptionable_id NOT IN (618193, 212916, 4733726, 2487148, 4684900)
      AND subscriptionable_id NOT IN (
        SELECT distinct user_id
        FROM sales_purchased_contents
        WHERE valid_until > now()
        AND status = 'active'
      )
    `
  )

  console.log(`-> ${subscriptions.length} assinaturas encontradas`)

  return subscriptions
}

async function findSubscriptionUsers(subscriptions: EdukSubscription[]) {
  console.log('==> Buscando os usuários das assinaturas')

  const userIds = subscriptions.map(({ subscriptionable_id }) => subscriptionable_id)

  const users = await usersDb
    .select('id', 'name', 'email', 'birthday', 'terms_of_use_accepted_at')
    .from('v1_user')
    .whereIn('id', userIds)

  console.log(`-> ${users.length} usuários encontrados`)

  return users
}

async function findCreditCardBySubscriptions(subscriptions: EdukSubscription[]) {
  console.log('==> Buscando cartões de crédito associados às assinaturas')
  const subscriptionIds = subscriptions.map(({ id }) => Number(id))

  const creditCardsQuery = paymentsDb
    .select(
      'shopper_reference',
      'holder_name',
      'last_digits',
      'brand',
      'expires_at_month',
      'expires_at_year',
      'gateway_recurrence_contract',
      'default',
      'shopper_reference_type',
      'created_at'
    )
    .from('creditcards')
    .where({
      gateway_id: 2,
      active: 1,
      good: 1,
      default: 1,
    })
    .andWhere(function () {
      this.where({ shopper_reference_type: 'subscription' }).orWhere({
        shopper_reference_type: null,
      })
    })
    .andWhereNot('gateway_recurrence_contract', null)
    .andWhere(function () {
      this.where(function () {
        this.where('expires_at_year', '>', 2022).orWhere(function () {
          this.where('expires_at_year', '>', 22).andWhere('expires_at_year', '<', 100)
        })
      }).orWhere(function () {
        this.whereIn('expires_at_year', [2022, 22]).andWhere('expires_at_month', '>=', 8)
      })
    })
    .whereIn('shopper_reference', subscriptionIds)

  // console.log(creditCardsQuery.toSQL())

  const creditCards = await creditCardsQuery
  console.log(`-> ${JSON.stringify(creditCards.length)} cartões de crédito encontrados`)

  return creditCards
}

async function findCreditCardBySubscriptionUsers(subscriptions: EdukSubscription[]) {
  console.log('==> Buscando cartões de crédito associados ao USUÁRIOS das assinaturas')
  const userIds = subscriptions.map(({ subscriptionable_id }) => subscriptionable_id)

  const creditCardsQuery = paymentsDb
    .select(
      'shopper_reference',
      'holder_name',
      'last_digits',
      'first_digits',
      'brand',
      'expires_at_month',
      'expires_at_year',
      'gateway_reference',
      'gateway_recurrence_contract',
      'default',
      'shopper_reference_type',
      'created_at'
    )
    .from('creditcards')
    .where({
      shopper_reference_type: 'user',
      gateway_id: 2,
      active: 1,
      good: 1,
      default: 1,
    })
    .andWhereNot('gateway_recurrence_contract', null)
    .andWhere(function () {
      this.where(function () {
        this.where('expires_at_year', '>', 2022).orWhere(function () {
          this.where('expires_at_year', '>', 22).andWhere('expires_at_year', '<', 100)
        })
      }).orWhere(function () {
        this.whereIn('expires_at_year', [2022, 22]).andWhere('expires_at_month', '>=', 8)
      })
    })
    .whereIn('shopper_reference', userIds)

  // console.log(creditCardsQuery.toSQL())

  const creditCards = await creditCardsQuery

  console.log(`-> ${JSON.stringify(creditCards.length)} cartões de crédito encontrados`)

  return creditCards
}

function associateSubscriptionWithCreditCards(
  activeSubscriptions: any[],
  creditCards: any[],
  usersCreditCards: any[],
  users: any[]
): MigrationData[] {
  console.log('==> Associando assinaturas com os respectivos cartões de crédito')
  const migrationData: any[] = []

  activeSubscriptions.forEach((subscription) => {
    const subscriptionCreditCards = creditCards.filter(
      (creditCard) => Number(creditCard.shopper_reference) === Number(subscription.id)
    )

    const user = users.find(
      (user) => Number(user.id) === Number(subscription.subscriptionable_id)
    )

    if (user) {
      const userCreditCards = usersCreditCards.filter(
        (creditCard) =>
          Number(creditCard.shopper_reference) ===
          Number(subscription.subscriptionable_id)
      )

      const finalCreditCardList = subscriptionCreditCards.concat(userCreditCards)

      defineUniqueMainCard(finalCreditCardList)

      migrationData.push({
        subscription: mapToSubskriboSubscription(subscription),
        user: mapToOrkestroUser(user),
        creditCards: mapToPagojCreditCard(finalCreditCardList),
      })
    }
  })

  console.log(`-> ${migrationData.length} assinaturas`)

  return migrationData
}

function defineUniqueMainCard(creditCards: EdukCreditCard[]) {
  if (creditCards.length > 0) {
    const sortedCreditCards = creditCards.sort(
      (a, b) => a.created_at.getTime() - b.created_at.getTime()
    )

    const mainCreditCard = sortedCreditCards.find(
      (creditCard) => (creditCard.default = 1)
    )

    sortedCreditCards.forEach((creditCard) => {
      creditCard.default = 0
    })

    if (mainCreditCard) {
      mainCreditCard.default = 1
    } else {
      sortedCreditCards[0].default = 1
    }
  }
}

function mapToOrkestroUser(edukUser: EdukUser): OrkestroUser {
  return {
    eduk_id: edukUser.id,
    name: edukUser.name,
    email: edukUser.email,
    birthdate: edukUser.birthday,
    privacy_terms_accepted_at: edukUser.terms_of_use_accepted_at || new Date(),
    origin_application: 'eduk',
  }
}

function mapToSubskriboSubscription(
  edukSubscription: EdukSubscription
): SubskriboSubscription {
  return {
    legacy_id: edukSubscription.id,
    active_until: edukSubscription.due_date || subDays(new Date(), 2),
    next_due_date: edukSubscription.due_date || subDays(new Date(), 2),
    plan_id: PLAN_ID as string,
    plan_profile_id: PLAN_PROFILE_ID as string,
    begin_date: edukSubscription.begin_date,
  }
}

function mapToPagojCreditCard(edukCreditCard: EdukCreditCard[]): PagojCreditCard[] {
  return edukCreditCard.map((creditCard) => ({
    gateway_shopper_reference: buildGatewayShopperReference(creditCard),
    brand: creditCard.brand,
    expiration_date: `${creditCard.expires_at_month}/${creditCard.expires_at_year}`,
    holder_name: creditCard.holder_name || '',
    is_main: creditCard.default === 1,
    gateway_reference: creditCard.gateway_recurrence_contract,
    last_four_digits: creditCard.last_digits,
  }))
}

function buildGatewayShopperReference(creditCard: EdukCreditCard) {
  if (creditCard.shopper_reference_type === 'user') {
    return `user-${creditCard.shopper_reference}`
  }

  if (creditCard.shopper_reference_type === 'subscription') {
    return `subscription-${creditCard.shopper_reference}`
  }

  return creditCard.shopper_reference
}

async function migrateData(migrationData: MigrationData[]) {
  await Promise.all(
    migrationData.map(async ({ user, subscription, creditCards }) => {
      const orkestroUser = await migrateUserToOkestro(user)

      const userHasMainCard = hasMainCard(creditCards)

      const subskriboUser = await migrationUserToSubskribo(orkestroUser, userHasMainCard)

      subscription.user_id = orkestroUser.id
      await migrateSubscription(subscription, userHasMainCard)

      await migrateCreditCards(creditCards, subskriboUser.id)
    })
  )
}

async function migrateUserToOkestro(user: OrkestroUser): Promise<OrkestroUser> {
  const [orkestroUser] = await orkestroDb
    .from<OrkestroUser>('users')
    .where({ eduk_id: String(user.eduk_id) })

  if (orkestroUser) {
    return orkestroUser
  }

  const [createdOrkestroUser] = await orkestroDb.insert(user, '*').into('users')

  return createdOrkestroUser
}

function hasMainCard(creditCards: PagojCreditCard[]) {
  return creditCards.some(({ is_main }) => is_main)
}

async function migrationUserToSubskribo(user: OrkestroUser, hasMainCard: boolean) {
  const [subskriboUser] = await subskriboDb.from('users').where({ id: user.id })

  if (subskriboUser) {
    return subskriboUser
  }

  const [createdSubskriboUser] = await subskriboDb
    .insert<SubskriboUser>({
      id: user.id as string,
      name: user.name,
      email: user.email,
      has_main_card: hasMainCard,
    })
    .into('users')
    .returning('*')

  return createdSubskriboUser
}

async function migrateSubscription(
  subscription: SubskriboSubscription,
  hasMainCard: boolean
) {
  const [subskriboSubscription] = await subskriboDb.from('subscriptions').where({
    user_id: subscription.user_id,
    plan_profile_id: subscription.plan_profile_id,
  })

  if (subskriboSubscription) {
    return subskriboSubscription
  }

  subscription.next_due_date = hasMainCard ? subscription.next_due_date : null

  const [createdSubskriboSubscription] = await subskriboDb
    .insert({
      user_id: subscription.user_id,
      active_until: subscription.active_until,
      next_due_date: subscription.next_due_date,
      plan_id: subscription.plan_id,
      plan_profile_id: subscription.plan_profile_id,
    })
    .into('subscriptions')
    .returning('*')

  const now = new Date()
  await subskriboDb
    .insert({
      plan_id: subscription.plan_id,
      activation_type: 'ACQUISITION',
      trigger: 'ACQUISITION',
      subscription_start_date: subscription.begin_date,
      subscription_end_date: subscription.active_until,
      payment_method: 'CREDIT_CARD',
      subscription_status: subscription.active_until > now ? 'ACTIVE' : 'SUSPENDED',
      automatic_renewal: !!subscription.next_due_date,
      status: 'done',
      order_id: null,
      subscription_id: createdSubskriboSubscription.id,
    })
    .into('subscription_history')

  await subskriboDb
    .insert({
      subscription_id: createdSubskriboSubscription.id,
      legacy_subscription_id: subscription.legacy_id,
    })
    .into('legacy_migrated_subscriptions')

  return createdSubskriboSubscription
}

async function migrateCreditCards(creditCards: PagojCreditCard[], shopperId: string) {
  const [mainCard] = await pagojDb
    .from('cards')
    .where({ shopper_id: shopperId, is_main: true })

  if (!mainCard) {
    creditCards.forEach((creditCard) => {
      creditCard.shopper_id = shopperId
    })

    await pagojDb.batchInsert('cards', creditCards)
  }
}

async function destroyConnections() {
  await Promise.all(connections.map((connection) => connection.destroy()))
}

main()
