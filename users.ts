import knex from 'knex'
import { connectionsConfig } from './connections.config'
import fs from 'fs'

const environment = process.env.ENVIRONMENT

type Subscription = {
  id: string
  subscriptionable_id: string
}

type EdukUser = {
  id: string
  name: string
  email: string
  birthday: Date
  terms_of_use_accepted_at: Date | null
}

type OrkestroUser = {
  id?: string
  eduk_id: string
  name: string
  email: string
  birthdate: Date
  privacy_terms_accepted_at: Date
  deleted_at?: Date
  origin_application: 'eduk'
}

type SubskriboUser = { id: string; name: string; email: string }

const paymentsDb = knex({
  client: 'mysql',
  connection: connectionsConfig.payments,
})

const subscriptionsDb = knex({
  client: 'pg',
  connection: connectionsConfig.subscriptions,
  searchPath: ['knex', 'public'],
})

const usersDb = knex({
  client: 'pg',
  connection: connectionsConfig.users,
  searchPath: ['knex', 'public'],
})

const orkestroDb = knex({
  client: 'pg',
  connection: connectionsConfig.orkestro,
  searchPath: ['knex', 'public'],
})

const subskriboDb = knex({
  client: 'pg',
  connection: connectionsConfig.subskribo,
  searchPath: ['knex', 'public'],
})

const connections = [paymentsDb, subscriptionsDb, usersDb, orkestroDb, subskriboDb]

async function main() {
  console.log(`==> Executando em --> ${environment} <--`)

  const activeSubscriptions = await findActiveSubscriptions()
  const edukSubscriptionUsers = await findSubscriptionUsers(activeSubscriptions)
  const notSynchronizedEdukUsers = await filterNotSynchronizedOkestroUsers(
    edukSubscriptionUsers
  )

  // const usersToInsert = mapToOrkestroUser(notSynchronizedEdukUsers)

  // const createdOrkestroUsers = await migrateUsersToOrkestro(usersToInsert.splice(0, 1))

  // console.log(createdOrkestroUsers)

  // const subskriboUsers = mapToSubskriboUser(createdOrkestroUsers)

  // const createSubskriboUsers = await migrateUsersToSubskribo(subskriboUsers)

  // console.log(createSubskriboUsers)

  // ======

  await destroyConnections()
}

async function findActiveSubscriptions() {
  console.log('==> Buscando todas as assinaturas ativas')

  const { rows: edukSubscriptions } = await subscriptionsDb.raw<{
    rows: { subscriptionable_id: number }[]
  }>(
    `
    SELECT distinct subscriptionable_id
    FROM subscriptions 
    WHERE status = 'active' 
    
    EXCEPT 
    
    SELECT distinct user_id 
    FROM sales_purchased_contents 
    WHERE valid_until > now() AND valid_until < '2025-01-01' AND status = 'active'
    `
  )

  const subscriptionsUserIds = edukSubscriptions.map(
    ({ subscriptionable_id }) => subscriptionable_id
  )

  const subscriptions = await subscriptionsDb
    .select('id', 'subscriptionable_id', 'plan_profile_id', 'due_date')
    .from('subscriptions')
    .whereIn('subscriptionable_id', subscriptionsUserIds)
    .andWhereRaw('plan_profile_id <> 99')
    .orderBy('created_at', 'desc')

  console.log(`-> ${subscriptions.length} assinaturas encontradas`)

  return subscriptions
}

async function findSubscriptionUsers(subscriptions: Subscription[]) {
  console.log('==> Buscando os usuários das assinaturas')

  const userIds = subscriptions.map(({ subscriptionable_id }) => subscriptionable_id)

  const users = await usersDb
    .select('id', 'name', 'email', 'birthday', 'terms_of_use_accepted_at')
    .from('v1_user')
    .whereIn('id', userIds)

  console.log(`-> ${users.length} usuários encontrados`)

  return users
}

async function filterNotSynchronizedOkestroUsers(edukUsers: EdukUser[]) {
  console.log('==> Removendo os usuários que já estão sincronizados com o orkestro')

  const edukUserIds = edukUsers.map((edukUser) => edukUser.id)

  const edukUserInOrkestro = await orkestroDb
    .from<OrkestroUser>('users')
    .whereIn('eduk_id', edukUserIds)
    .andWhere('deleted_at', null)

  console.log(`-> ${edukUserInOrkestro.length} usuário removidos`)

  const notSynchronizedEdukUsers = edukUsers.filter(
    (edukUser) =>
      !edukUserInOrkestro.some(({ eduk_id }) => Number(eduk_id) === Number(edukUser.id))
  )

  console.log(`-> ${notSynchronizedEdukUsers.length} usuário não sincronizados`)
  fs.writeFileSync('edukUserInOrkestro.json', JSON.stringify(edukUserInOrkestro))

  return notSynchronizedEdukUsers
}

function mapToOrkestroUser(edukUsers: EdukUser[]): OrkestroUser[] {
  return edukUsers.map((edukUser) => ({
    eduk_id: edukUser.id,
    name: edukUser.name,
    email: edukUser.email,
    birthdate: edukUser.birthday,
    privacy_terms_accepted_at: edukUser.terms_of_use_accepted_at || new Date(),
    origin_application: 'eduk',
  }))
}

function mapToSubskriboUser(orkestroUsers: OrkestroUser[]): SubskriboUser[] {
  return orkestroUsers.map((orkestroUser) => ({
    id: orkestroUser.id as string,
    name: orkestroUser.name,
    email: orkestroUser.email,
  }))
}

async function migrateUsersToSubskribo(users: SubskriboUser[]) {
  return await subskriboDb.batchInsert('users', users).returning('*')
}

async function migrateUsersToOrkestro(users: OrkestroUser[]) {
  return orkestroDb.batchInsert('users', users).returning('*')
}

async function destroyConnections() {
  await Promise.all(connections.map((connection) => connection.destroy()))
}

main()
